import random, functools, math, pandas as pd, matplotlib, queue, itertools, os
import enum, numpy as np
from datetime import timedelta, datetime

class User(enum.Enum):
    STUDENT=1
    SMALL_JOB=2
    MED_JOB=3
    LARGE_JOB=4
class InterruptPolicy(enum.Enum):
    SAVE=0
    DELETE=1
class SchedulerPolicy(enum.Enum):
    FIFO=0
    SJF=1
    SRTF=2
    RR=3
    LRTF=4

class ReversePriorityQueue(queue.PriorityQueue):
    def put(self, tup):
        newtup = tup[0] * -1, tup[1]
        queue.PriorityQueue.put(self, newtup)
    def get(self):
        tup = queue.PriorityQueue.get(self)
        newtup = tup[0] * -1, tup[1]
        return newtup
    def get_nowait(self):
        tup = queue.PriorityQueue.get(self)
        newtup = tup[0] * -1, tup[1]
        return newtup

#SIMULATOR OPTIONS
NUM_CPUS = 20
CHANCE_OF_CONTINUED_USE = 71 #chance of a user staying on the machine through a time block

START_TIME = datetime.strptime('04-13-2020 00:00', '%m-%d-%Y %H:%M')
END_TIME = datetime.strptime('04-13-2020 23:59', '%m-%d-%Y %H:%M') #the sim will change this to correspond to the number of days it runs for

MIN_DELTA = timedelta(seconds=60)
ZERO_DELTA = timedelta(seconds=0)
SMALL_JOB_TIME = MIN_DELTA*15
MED_JOB_TIME = MIN_DELTA*45
LARGE_JOB_TIME = MIN_DELTA*180
SWITCH_TIME = MIN_DELTA*2 #how long it takes to switch between student and research VM

SCHEDULER_POLICY = SchedulerPolicy.SRTF
INTERRUPT_POLICY = InterruptPolicy.SAVE
SWITCH = False #switch this value to True with an SRTF or LRTF policy if you want to switch between them

NUM_SMALL_JOBS = 1000
NUM_MED_JOBS = 1000
NUM_LARGE_JOBS = 1000

class Computer:
    def __init__(self):
        self.user = 0
        self.reserved = False #used to see when we're switching to research VM
        self.reserved_time = None
        self.job = None
        self.job_start_time = None
class Job:
    def __init__(self, ttime, size):
        self.size=size
        self.total_time = ttime
        self.time_remaining = ttime
        self.start_time = None
        self.completion_time = None
        self.times_stopped = 0
    def __lt__(self, other):
        if(SCHEDULER_POLICY == SchedulerPolicy.SJF):
            return self.total_time < other.total_time
        else:
            return self.time_remaining < other.time_remaining

#output is list of times per user that average to total %time used from input
#ex: [5, 3, 4] from 0...n = use0 stayed for 5 min, user1 for 3 min, usern for 4 min
def genUsers(expected_avg, n):
    choices = [x for x in range (5, 16)] #5-15 because we assume nobody logged on for < 5 minutes (if it must be under it will assign 5 minutes duration)
    while True:
        if expected_avg == 0 or n == 0: #nobody is here
            return [0,]
        elif (expected_avg <= 5): #we just assume everyone was here for 5 minutes into this time block for simplicity
            return [5 for x in range(n)]
        elif n==1:
            return [math.ceil(expected_avg)]
        elif n >= 2:
            l = [random.choice(choices) for i in range(n)] #choose n values from the choices list randomly
            avg = functools.reduce(lambda x, y: x + y, l) / len(l) # assign those values such that they equal the ceiling of the expected average
            if math.ceil(avg) in range(math.ceil(expected_avg) -1, math.ceil(expected_avg) + 1): #return if successful, else retry
                return l

#make a datafrom where the index/first column is time, and each column header following is Computer_0...Computer_n
#for each minute, a computer column will contain a 1 if it is used
def makeUsageDataframe(st_time):
    df = pd.read_csv('avg_usage_values.csv')
    df.columns = ['num_users', 'avg_use_of_time']
    df['time'] = pd.date_range(st_time, periods=96, freq='15min')

    usage_df = pd.DataFrame() 
    usage_df['time'] = pd.date_range(st_time, periods=1440, freq='1min') # one row for each minute
    usage_df.index = usage_df['time'] #time is index

    for i in range(NUM_CPUS):
        usage_df[f'Computer_{i}'] = 0 #1 col for each computer initialized to 0
    last_vals = [x for x in range(NUM_CPUS)] #taken cpu list for last iteration
    for i in df.index:
        expected_avg= 15*df.at[i, 'avg_use_of_time'] + float(random.random() * random.randint(-1,1))
        n = df.at[i, 'num_users']
        user_length = genUsers(expected_avg, n) #returns list where each entry is number of minutes spent in lab by a user
        curr_time=df.at[i,'time']
        choices = [x for x in range(NUM_CPUS)]
        chosen_vals = []
        for j in user_length: #for each person in the lab
            if len(last_vals) is 0:
                last_vals = choices
            if random.randint(1,101) < CHANCE_OF_CONTINUED_USE: #assign person to a computer already in use
                val = random.choice(last_vals)
                last_vals.remove(val)
                if val in choices:
                    choices.remove(val)
            else: #assign randomly
                val = random.choice(choices)
                choices.remove(val)
                if val in last_vals:
                    last_vals.remove(val)
            chosen_vals.append(val) #chosen vals = computers assinged
            for x in range(j):
                usage_df.at[(curr_time + timedelta(seconds=x*60)),f'Computer_{val}'] = 1
        last_vals=chosen_vals
    return usage_df

#function to save graphs for this simulated run
def makePlots(df):
    df_student = df.copy()
    df_student.replace(2,0, inplace=True)
    df_student.replace(3,0, inplace=True)
    df_student.replace(4,0, inplace=True)

    df_short = df.copy()
    df_short.replace(1,0, inplace=True)
    df_short.replace(3,0, inplace=True)
    df_short.replace(4,0, inplace=True)
    df_short.replace(2,1, inplace=True)

    df_medium = df.copy()
    df_medium.replace(1,0, inplace=True)
    df_medium.replace(2,0, inplace=True)
    df_medium.replace(4,0, inplace=True)
    df_medium.replace(3,1, inplace=True)

    df_long = df.copy()
    df_long.replace(1,0, inplace=True)
    df_long.replace(2,0, inplace=True)
    df_long.replace(3,0, inplace=True)
    df_long.replace(4,1, inplace=True)

    df_unused = df.copy()
    df_unused.replace(1,5, inplace=True)
    df_unused.replace(0,1, inplace=True)
    df_unused.replace(2,0, inplace=True)
    df_unused.replace(3,0, inplace=True)
    df_unused.replace(4,0, inplace=True)
    df_unused.replace(5,0, inplace=True)

    usage_avg = {}
    for col in df_student.columns:
        usage_avg.update({col:df_student[col].mean()*100})    
    usage_avg_df = pd.DataFrame()
    usage_avg_df['student_percent'] = pd.Series(usage_avg)

    usage_avg = {}
    for col in df_short.columns:
        usage_avg.update({col:df_short[col].mean()*100})
    usage_avg_df['short_job_percent'] = pd.Series(usage_avg)

    usage_avg = {}
    for col in df_medium.columns:
        usage_avg.update({col:df_medium[col].mean()*100})
    usage_avg_df['medium_job_percent'] = pd.Series(usage_avg)

    usage_avg = {}
    for col in df_long.columns:
        usage_avg.update({col:df_long[col].mean()*100})
    usage_avg_df['long_job_percent'] = pd.Series(usage_avg)

    usage_avg = {}
    for col in df_unused.columns:
        usage_avg.update({col:df_unused[col].mean()*100})
    usage_avg_df['unused_percent'] = pd.Series(usage_avg)

    if not os.path.exists('graphs'):
        os.makedirs('graphs')

    usage_avg_df.plot(kind='bar', stacked=True, figsize=(25,7))
    filename = f'graphs/{SCHEDULER_POLICY.name}_'
    if(SWITCH):
        filename += 'SWITCH_'
    matplotlib.pyplot.savefig(f'{filename}{INTERRUPT_POLICY.name}_avg_usage_by_cpu.png')

    student_series = df_student.sum(axis=1)
    short_series = df_short.sum(axis=1)
    medium_series = df_medium.sum(axis=1)
    long_series = df_long.sum(axis=1)
    unused_series = df_unused.sum(axis=1)

    window = 15
    job_comparison_mva = pd.DataFrame()
    job_comparison_mva['student'] =  np.ceil(student_series.rolling(window=window).mean())
    job_comparison_mva['short'] =  np.ceil(short_series.rolling(window=window).mean())
    job_comparison_mva['medium'] =  np.ceil(medium_series.rolling(window=window).mean())
    job_comparison_mva['long'] =  np.ceil(long_series.rolling(window=window).mean())

    job_comparison_mva.plot(kind='line', figsize=(25,7))
    filename = f'graphs/{SCHEDULER_POLICY.name}_'
    if(SWITCH):
        filename += 'SWITCH_'
    matplotlib.pyplot.savefig(f'{filename}{INTERRUPT_POLICY.name}_avg_usage_by_time.png')

#debug print to what ran at what time range per CPU
def printActiveTimes(data):
    for cpu in data.keys():
        i = START_TIME
        print(f'{cpu} active times: ')
        while i < END_TIME:
            in_range = data[cpu][i]
            if(in_range == 1):
                strrange = f'\tstudent: {i} - '
                while i < END_TIME and data[cpu][i] == 1:
                    i+=MIN_DELTA
                strrange += f'{i - MIN_DELTA}'
                print(strrange)
            elif(in_range == 2):
                strrange = f'\tsmall job: {i} - '
                while i < END_TIME and data[cpu][i] == 2:
                    i+=MIN_DELTA
                strrange += f'{i - MIN_DELTA}'
                print(strrange)
            elif(in_range == 3):
                strrange = f'\tmedium job: {i} - '
                while i < END_TIME and data[cpu][i] == 3:
                    i+=MIN_DELTA
                strrange += f'{i - MIN_DELTA}'
                print(strrange)
            elif(in_range == 4):
                strrange = f'\tlarge job: {i} - '
                while i < END_TIME and data[cpu][i] == 4:
                    i+=MIN_DELTA
                strrange += f'{i - MIN_DELTA}'
                print(strrange)
            else:
                i+=MIN_DELTA

#run simulator on dictionary formatted like {computer->{time:used}}
#will return a dictionary where the usage value is 1, 2, 3, or 4 depending on
#what has been scheduled at that time
def runSim(data, job_queue):
    global SCHEDULER_POLICY
    job_completion_list = []
    cpu_lab = [Computer() for x in range(NUM_CPUS)]
    t = START_TIME
    while t <= END_TIME:
        if(SWITCH and SCHEDULER_POLICY == SchedulerPolicy.SRTF and t.hour >= 1 and t.hour < 9):
            SCHEDULER_POLICY = SchedulerPolicy.LRTF
            temp_q = ReversePriorityQueue()
            while(not job_queue.empty()):
                j = job_queue.get_nowait()
                temp_q.put((j[0], j[1]))
            job_queue = temp_q
        if(SWITCH and SCHEDULER_POLICY == SchedulerPolicy.LRTF and t.hour >= 9):
            SCHEDULER_POLICY = SchedulerPolicy.SRTF
            temp_q = queue.PriorityQueue()
            while(not job_queue.empty()):
                j = job_queue.get_nowait()
                temp_q.put((j[0], j[1]))
            job_queue = temp_q
        cpus = list(data.keys())
        for i in range(len(cpus)):
            cpu = cpu_lab[i]
            if (data[cpus[i]][t] == 1):
                cpu.user = User.STUDENT
                if(cpu.reserved):
                    cpu.reserved = False
                    cpu.reserved_time = None
                elif(cpu.job != None):
                    cpu.job_start_time = None
                    if(INTERRUPT_POLICY == InterruptPolicy.DELETE):
                        cpu.job.time_remaining = cpu.job.total_time
                    cpu.job.times_stopped += 1
                    if(SCHEDULER_POLICY == SchedulerPolicy.SRTF or SCHEDULER_POLICY == SchedulerPolicy.LRTF):
                        job_queue.put((cpu.job.time_remaining, cpu.job))
                    elif(SCHEDULER_POLICY == SchedulerPolicy.SJF):
                        job_queue.put((cpu.job.total_time, cpu.job))
                    else:
                        job_queue.put((None, cpu.job))
                    cpu.job=None
            else:
                if(cpu.job==None and not cpu.reserved):
                    cpu.reserved = True
                    cpu.reserved_time = t
                    cpu.user = None
                elif(cpu.reserved and t - cpu.reserved_time >= SWITCH_TIME):
                    cpu.reserved = False
                    try:
                        cpu.job = job_queue.get_nowait()[1]
                    except:
                        continue
                    cpu.job_start_time = t
                    if(cpu.job.start_time == None):
                        cpu.job.start_time = t
                    cpu.user = cpu.job.size
                    data[cpus[i]][t] = cpu.job.size.value
                elif(cpu.job != None):
                    cpu.job.time_remaining -= MIN_DELTA
                    data[cpus[i]][t] = cpu.job.size.value
                    if(cpu.job.time_remaining == ZERO_DELTA):
                        cpu.job.completion_time = t
                        job_completion_list.append(cpu.job)
                        cpu.job = None
                        cpu.job_start_time = None
            # print(f'{t} | CPU {i}: user {cpu.user}, reserved {cpu.reserved}, {cpu.job.size if cpu.job != None else None}')
        t += MIN_DELTA
    return data, job_queue, job_completion_list

def main():
    query=input("Make new data? (y/n): ")
    n_days = input("How many days? ")
    if('y' in query):
        if not os.path.exists('matrices'):
            os.makedirs('matrices')
        for i in range(int(n_days)):
            df = makeUsageDataframe((START_TIME + (i *MIN_DELTA*60*24)).strftime('%m-%d-%Y %H:%M'))
            df.to_csv(f'matrices/Student_Usage_Matrix_{i}.csv')
    global END_TIME
    END_TIME = START_TIME + ((MIN_DELTA*60*24*int(n_days)) - MIN_DELTA)
    df = pd.concat([pd.read_csv(f'matrices/Student_Usage_Matrix_{i}.csv', index_col='time', parse_dates=['time', 'time.1']).rename(columns={'time.1':'time'}) for i in range(int(n_days))])
    student_usage_data = df.drop(columns="time").to_dict()
    job_queue = queue.Queue()
    index = 0
    if(SCHEDULER_POLICY == SchedulerPolicy.SRTF or SCHEDULER_POLICY == SchedulerPolicy.SJF):
        job_queue = queue.PriorityQueue()
        while index < max(NUM_SMALL_JOBS, NUM_MED_JOBS, NUM_LARGE_JOBS):
            if(index < NUM_SMALL_JOBS):
                job_queue.put((SMALL_JOB_TIME, Job(SMALL_JOB_TIME, User.SMALL_JOB)))
            if(index < NUM_MED_JOBS):
                job_queue.put((MED_JOB_TIME, Job(MED_JOB_TIME, User.MED_JOB)))
            if(index < NUM_LARGE_JOBS):
                job_queue.put((LARGE_JOB_TIME, Job(LARGE_JOB_TIME, User.LARGE_JOB)))
            index+=1
    else:
        while index < max(NUM_SMALL_JOBS, NUM_MED_JOBS, NUM_LARGE_JOBS):
            if(index < NUM_SMALL_JOBS):
                job_queue.put((None, Job(SMALL_JOB_TIME, User.SMALL_JOB)))
            if(index < NUM_MED_JOBS):
                job_queue.put((None, Job(MED_JOB_TIME, User.MED_JOB)))
            if(index < NUM_LARGE_JOBS):
                job_queue.put((None, Job(LARGE_JOB_TIME, User.LARGE_JOB)))
            index+=1

    data, job_queue, jobs_completed = runSim(student_usage_data, job_queue)
    
    # for j in range(len(list(job_queue.queue))):
    #     print(job_queue.get()[1].time_remaining)
    # printActiveTimes(data)

    small_jobs_completed = 0
    med_jobs_completed = 0
    large_jobs_completed = 0
    num_interruptions = 0
    jobs_interrupted = 0
    jobs_worked = len(jobs_completed)
    for j in jobs_completed:
        if(j.times_stopped > 0):
            jobs_interrupted += 1
        num_interruptions += j.times_stopped
        if(j.size == User.SMALL_JOB):
            small_jobs_completed += 1
        elif(j.size == User.MED_JOB):
            med_jobs_completed += 1
        else:
            large_jobs_completed += 1

    for j in job_queue.queue:
        if(j[1].start_time != None):
            jobs_worked += 1
            num_interruptions += j[1].times_stopped
            jobs_interrupted += 1

    print(f'{SCHEDULER_POLICY.name}, {INTERRUPT_POLICY.name} : {len(jobs_completed)} jobs completed: {small_jobs_completed} small, {med_jobs_completed} med, {large_jobs_completed} large. {num_interruptions} total interruptions, {(jobs_interrupted/jobs_worked)*100} % interrupted')
    new_df = pd.DataFrame.from_dict(data)
    makePlots(new_df)

    new_df.to_csv(f'matrices/Usage_Matrix_{SCHEDULER_POLICY.name}_{INTERRUPT_POLICY.name}.csv')


if __name__ == "__main__":
    main()


import os, stat, time

JOB_DIRECTORY = 'Jobs'
RESULT_DIRECTORY = 'Results'

def run_client():


    for filename in os.listdir(JOB_DIRECTORY):
        if filename.endswith(".py"):
            result_file = RESULT_DIRECTORY + "/" + os.path.splitext(filename)[0]
            os.system("chmod +x " + JOB_DIRECTORY + "/" + filename)
            os.system(JOB_DIRECTORY + "/" + filename +" > " + result_file)
            os.remove(JOB_DIRECTORY + "/" + filename)
        else:
            os.system("mv " + JOB_DIRECTORY + "/" + os.path.splitext(filename)[0] + " " + JOB_DIRECTORY + "/" + os.path.splitext(filename)[0] + ".py")
            continue

# def send_results():


if __name__ == "__main__":
    try:
        while True:
            run_client()
            time.sleep(1)
    except KeyboardInterrupt:
        print("Int received: shutting down...")


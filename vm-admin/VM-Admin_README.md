# VM Admin Commands/Cheat Sheet

### This will guide you through:
    1. Installing VM From Image
    2. Creating VM's
    3. Connecting to VM's
    4. Sending job scripts to VM's
    5. Saving State on a VM
    6. Shutting Down VM

## Install Image
https://gtvault-my.sharepoint.com/:f:/g/personal/pstephens30_gatech_edu/EpLvo8I0DgZDhC4ZE53lLw0BeZ5w9nyC3zKKk8c5CqJJVQ?e=sfd8ep
This link includes a qcow disk file and an XML settings file
IMPORTANT: Place both in var/lib/uvtool/libvirt/images
Note: If you can't place them in this folder, that's okay, but you'll need to change the qcow folder in the XML settings  
Import with `virsh define client1-clone.xml`  
Start with: `virsh start client1-clone`


## Creating VM's
Taken from CS 6210 and https://help.ubuntu.com/lts/serverguide/cloud-images-and-uvtool.html  
Install uvtool: `sudo apt -y install uvtool`   
Download VM OS Image (might take a few minutes): `uvt-simplestreams-libvirt sync release=xenial arch=amd64`  
Generate SSH Key: `ssh-keygen`  
Create VM: `uvt-kvm create *name* release=xenial`  
To make sure your VM started correctly, list out VM's: `virsh list --all`  

## Connecting to VM's
There are 2 ways to connect to a VM with SSH
To connect to local VM's: `uvt-kvm ssh *vm_name* --insecure` (Note: asteriks not apart of command)  
Get Node IP Address: `virsh net-dhcp-leases default`  
To connect to remote VM: `ssh username@ip_address (username is ubuntu in my instances)`

## Sending job scripts to VM's
https://stackoverflow.com/questions/13928116/write-a-shell-script-to-ssh-to-a-remote-machine-and-execute-commands  

Send/Execute Script on VM: `ssh username@ip_address 'bash -s' < local_script.sh` (Note: where local_script is a bash script)

## Saving State on a VM
Save State: `virsh save *domain_name* *domain_state_file* --verbose --running`  (Note: Like putting computer in sleep, saves memory to a file=domain_state_file)  
Restore State: `virsh restore *same_domain_state_file_as_above*`

Storage/Disk File (.qcow) for Domains Saved to (Default): `/var/lib/uvtool/libvirt/images`

## Shutting Down VM
To Shutdown VM: `virsh shutdown *vm_name*`  
To Destroy Storage Pool (analogous to a hard drive/persistent storage): `virsh undefine *vm_name* --remove-all-storage` 

## Cloning a VM
`virsh dumpxml client1-clone > /var/lib/uvtool/libvirt/metadata/client1-clone.xml`
Use `virt-clone ...` to clone


import socket, os, time
from Logging import Logging
from settings import Settings

BUFFER_SIZE = 1024


class admin_commander_comms:
    @staticmethod
    def multiplex_message(server: socket):
        print("listening for new connections")
        server.listen()
        print("listen finished")
        s, discard = server.accept()
        print("connection accepted")
        time.sleep(1)
        data = s.recv(2048).decode()
        print(data)
        header = data.split(')')[0]
        header = str(header)[1:]
        header_length = len(header) + 2
        print("Header = " + str(header) + "\n Len = " + str(header_length))
        payload = data.split(')')[1:]
        

        header_type = header.split(',')[0]
        job_num = header.split(',')[1]
        if header_type == 'ENQUEUE':
            print("Enqueuing")
            payload = data [header_length + 1:-5]
            print("Payload = " + str(payload))
            admin_commander_comms.enque(s, job_num, payload.encode())
        elif header_type == 'QUERYRET':
            Logging.log("\n\nQuery Returned\n")
            payload = data [header_length:-5]
            print("Payload = " + str(payload))
            admin_commander_comms.inbound_comm(payload.split('_')[0], job_num)

        else:
            print("ERROR")

    @staticmethod
    def hello(s: socket):
        s.sendall("(0,HELLO)_quit".encode())
        s.close()

    @staticmethod
    def finished(s: socket, job_num:int):
        print("Finished " + str(job_num))
        string = "(" + str(job_num) + ",FINISHED)"
        data = string.encode()
        with open('VM_Results/' + str(job_num), 'rb') as f:
            data += f.read()
        data += '_quit'.encode()
        s.sendall(data)
        s.close()
    '''(job-number,type,opt3)
    <data>

    "PASS"        (indicates this vm-admin's job has sent a message to the server;
    <data> contains the message, opt3 is the FILE NAME [leave blank or non-numeric for publicly readable])'''
    @staticmethod
    def pass_msgs(s: socket):
        for comm in os.listdir(Settings.admin_outbound_comms_directory):
            with open(Settings.admin_outbound_comms_directory + comm, 'rb') as f:
                data = f.read()
            job_num = comm.split('_')[0]
            file_name = comm.split('_')[1]
            Logging.log("Passing message to commander")
            msg = str("(" + str(job_num) + ",PASS," + str(file_name) + ")").encode() + data + str("_quit").encode()
            s.sendall(msg)
            os.system("rm " + Settings.admin_outbound_comms_directory + comm)
        s.close()

        

# '''"QUERY"       (indicates this vm-admin's job is querying for the 
# file name given in <data> from the job number given in opt3 
# [leave blank to query all publicly readable files])'''

    @staticmethod
    def query(s: socket):
        for comm in os.listdir(Settings.admin_query_directory):
            query = comm.split('_')
            job_num = query[0]
            file_name = query[1]

            Logging.log("Querying for msg from Commander")
            msg = str("(" + str(job_num) + ",QUERY," + str(file_name) + ")").encode() + str("_quit").encode()
            s.sendall(msg)
            os.system("rm " + Settings.admin_query_directory + comm)
        s.close()


    @staticmethod
    def pausing(s: socket, job_num:int):
        string = "(" + str(job_num) + ",PAUSING)_quit"
        s.sendall(string.encode())

    @staticmethod
    def enque(s: socket, job_num:int, init_data):
        print("enqueing")
        filename = str(job_num)
        with open('Jobs/' + filename, 'wb') as f:
            print('file opened')
            print('init data=%s', (init_data))
            f.write(init_data)
            while True:
                print('receiving data...')
                data = s.recv(BUFFER_SIZE)
                print('data=%s', (data))
                if not data:
                    break
                # write data to a file
                f.write(data)
        f.close()
        s.close()
    @staticmethod
    def inbound_comm(data, job_num):

        with open('Inbound_Comms/' + str(job_num), 'w') as f:
            f.writelines("%s" % line for line in data[1:])
        
    


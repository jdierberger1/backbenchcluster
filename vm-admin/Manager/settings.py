class Settings:
    GUEST_VM_USERNAME_IPS = ["ec2-user@ec2-54-234-127-140.compute-1.amazonaws.com",
    ]
    PRIVATE_KEY_FILE = "KeyPair2.pem"
    # COMMANDER_IP_ADDRESS = "192.168.1.251"
    COMMANDER_IP_ADDRESS = "54.226.193.151"
    MY_IP_ADDRESS = "0.0.0.0"
    # MY_IP_ADDRESS = "0.0.0.0"
    COMMANDER_PORT = 32105
    BUFFER_SIZE = 1024

    admin_job_directory = './Jobs/'
    admin_result_directory = './VM_Results/'
    admin_outbound_comms_directory = './Outbound_Comms/'
    admin_inbound_comms_directory = './Inbound_Comms/'
    admin_query_directory = './Queries/'


#!/usr/bin/env python3
import os, socket, sys, select, queue as Queue, time
from typing import List
from Guest_VM import Guest_VM
from Job import Job
from settings import Settings
from Admin_Node_Comms import admin_node_comms
from Admin_Commander_Comms import admin_commander_comms



# CLIENT_IP_ADDRESS = {("0.0.0.0", 2000), ("0.0.0.0", 2001)}


def commander():
    commander = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    commander.connect((Settings.COMMANDER_IP_ADDRESS, Settings.COMMANDER_PORT))
    return commander;

try:
    os.system("rm Jobs/*")
    os.system("rm VM_Results/*")
    os.system("rm Queries/*")
    os.system("rm Outbound_Comms/*")
    os.system("rm Inbound_Comms/*")
    os.system("mkdir Jobs")
    os.system("mkdir VM_Results")
    os.system("mkdir Queries")
    os.system("mkdir Outbound_Comms")
    os.system("mkdir Inbound_Comms")

    print("Attempting connection & hello")
    admin_commander_comms.hello(commander())
    print("Said hello :)")
    admin_node_comms.setup_VMs()
    print("VM is setup")

    commander_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #commander_server.bind((Settings.MY_IP_ADDRESS, Settings.COMMANDER_PORT))
    commander_server.bind((Settings.MY_IP_ADDRESS, Settings.COMMANDER_PORT))
    commander_server.settimeout(7)

    while True:
        print("\n")
        try:
            admin_commander_comms.multiplex_message(commander_server)
        except:
            print("Socket Timed out")
        print("Received job")
        time.sleep(1)
        admin_node_comms.send_jobs()
        time.sleep(3)

        
        admin_node_comms.get_all_comms(commander())
        admin_node_comms.collect_queries()
        admin_commander_comms.pass_msgs(commander())
        admin_commander_comms.query(commander())
        admin_node_comms.send_back_msg()

        time.sleep(0)

        admin_node_comms.collect_results()
        admin_node_comms.send_results_to_commander(commander())


except KeyboardInterrupt:
    print("Keyboard Int Received")
    commander().close()
# except Exception as e:
#     print(e)


import socket
class Guest_VM:
    address:str = ""
    jobs:int = 0
    socket:socket = None

    def __init__(self, address):
        self.address = address
        self.jobs = 0
        self.socket = None
    
    def __str__(self):
        return "VM at address " + str(self.IP_Addr) + ":" + str(self.port) + "\n Job Count: " + str(self.jobs)
    
    def set_socket(self, socket: socket):
        self.socket = socket
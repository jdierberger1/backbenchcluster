import socket, os, time
import sys, select, queue as Queue
from typing import List
from Guest_VM import Guest_VM
from Job import Job
from settings import Settings
from Admin_Commander_Comms import admin_commander_comms
from Logging import Logging


class admin_node_comms:
    guest_vms: List[Guest_VM] = []
    jobs: List[Job] = []

    
    @staticmethod
    def setup_VMs():
        num_VMs = 1
        for i in range(0, num_VMs):
            #error_msg = os.system("virsh start client" + str(i))
            #if error_msg == 0: 
            vm = Guest_VM(Settings.GUEST_VM_USERNAME_IPS[i])
            admin_node_comms.guest_vms.append(vm)



    #Get VM with least # of jobs
    @staticmethod
    def get_VM_to_schedule() -> Guest_VM:
        def sort_by_job(vm: Guest_VM):
            return vm.jobs
        admin_node_comms.guest_vms.sort(key = sort_by_job)
        return admin_node_comms.guest_vms[0]

    @staticmethod
    def send_jobs():
        for job_fd in os.listdir(Settings.admin_job_directory):
            vm = admin_node_comms.get_VM_to_schedule()
            print("Sending to " + str(vm.address))
            os.system("scp -i "+ Settings.PRIVATE_KEY_FILE +" Jobs/" + job_fd + " " + str(vm.address) + ":~/Client/Jobs")
            os.system("rm Jobs/" + os.path.splitext(job_fd)[0])
            print("Done Sending")
            admin_node_comms.jobs.append(Job(os.path.splitext(job_fd)[0], vm))
            #os.system("rm Jobs/" + job_fd)
            

        
    @staticmethod
    def collect_results():
        print ("Collecting Results")
        for job in admin_node_comms.jobs:
            err_code = os.system("scp -i "+ Settings.PRIVATE_KEY_FILE + " " + str(job.vm_assigned_to.address) + ":~/Client/Results/" + job.file_path + "* VM_Results/")
            if err_code == 0:
                print("Results for " + job.file_path + " were successfully received")
                os.system("ssh -i " + Settings.PRIVATE_KEY_FILE + " " + job.vm_assigned_to.address + " \"rm Client/Results/\"" + job.file_path + "*")
                job.is_complete = True

    @staticmethod
    def send_results_to_commander(socket):
        for job in admin_node_comms.jobs:
            if job.is_complete:
                print("Completing Job: " + job.file_path)
                admin_commander_comms.finished(socket, job.file_path)
                os.system("rm VM_Results/" + job.file_path)
                admin_node_comms.jobs.remove(job)
    @staticmethod
    def get_all_comms(socket):
        print ("Collecting Messages")
        for vm in admin_node_comms.guest_vms:
            err_code = os.system("scp -i "+ Settings.PRIVATE_KEY_FILE + " " + str(vm.address) + ":~/Client/Outbound_Comms/* Outbound_Comms/")
            if err_code == 0:
                print("Msg received")
                os.system("ssh -i " + Settings.PRIVATE_KEY_FILE + " " + str(vm.address) + " \"rm Client/Outbound_Comms/*\"")
        

    @staticmethod
    def send_back_msg():
        print("Sending back")
        for comm in os.listdir(Settings.admin_inbound_comms_directory):
            print("made it here")
            for job in admin_node_comms.jobs:
                print("and here")
                print(job.file_path)
                if job.file_path == str(comm):
                    Logging.log("Sending back message to client")
                    os.system("scp -i "+ Settings.PRIVATE_KEY_FILE + " " + Settings.admin_inbound_comms_directory + "/"  + job.file_path + " "+ str(job.vm_assigned_to.address) + ":~/Client/Inbound_Comms/" + job.file_path)
                    os.system("rm " + Settings.admin_inbound_comms_directory + "/" + job.file_path)
                    break
    
    @staticmethod
    def collect_queries():
        print ("Collecting Queries")
        for job in admin_node_comms.jobs:
            err_code = os.system("scp -i "+ Settings.PRIVATE_KEY_FILE + " " + str(job.vm_assigned_to.address) + ":~/Client/Queries/* " + Settings.admin_query_directory)
            if err_code == 0:
                print("Queries for " + job.file_path + " were successfully received")
                os.system("ssh -i " + Settings.PRIVATE_KEY_FILE + " " + job.vm_assigned_to.address + " rm Client/Queries/*")
                os.system("ssh -i " + Settings.PRIVATE_KEY_FILE + " " + job.vm_assigned_to.address + " rm Client/Queries/*")
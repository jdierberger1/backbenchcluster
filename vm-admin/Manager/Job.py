from Guest_VM import Guest_VM
class Job:
    file_path: str = ""
    is_assigned:bool = False
    vm_assigned_to: Guest_VM = None
    is_complete = False

    def __init__(self, file_path: str, vm: Guest_VM = None):
        self.file_path = file_path
        self.is_complete = False
        if vm is None:
            self.is_assigned = False
            self.vm_assigned_to = None
        else:
            self.is_assigned = True
            self.vm_assigned_to = vm

    

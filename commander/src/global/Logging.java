package global;

public class Logging {

	public static final boolean doLogging = true;

	public static final void log(Object s) {
		if (doLogging) {
			System.out.println(s);
		}
	}

	public static final void logf(String s, Object...objects) {
		if (doLogging) {
			System.out.printf(s, objects);
		}
	}

}

package global;

public class Settings {

	public static final boolean DEBUG = true;

	public static final int NETWORK_PORT = 32105;

	public static final String DEPLOY_SPEC_FORMAT_STRING = "deployment_spec_%d.dat";

	public static final String SAVED_STATE_FORMAT_STRING = "saved_state_%d.dat";

}

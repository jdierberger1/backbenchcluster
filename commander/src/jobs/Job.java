package jobs;

import java.net.InetAddress;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import global.Settings;

public class Job {

	private static final Map<Integer, Job> jobs = Collections.synchronizedMap(new HashMap<>());

	private static int ID = 1;

	public static Job get(int jobNum) {
		return jobs.get(jobNum);
	}

	public static Job get(String jobNum) {
		try {
			return get(Integer.parseInt(jobNum));
		} catch (Exception e) {
			return null;
		}
	}

	private InetAddress address;

	private int jobNumber;

	private boolean hasRun;

	public Job() {
		address = null;
		jobNumber = ID++;
		hasRun = false;
		jobs.put(jobNumber, this);
	}

	public InetAddress getAddress() {
		return address;
	}

	public void setAddress(InetAddress addr) {
		this.address = addr;
	}

	public void setRun(boolean run) {
		hasRun = run;
	}

	public int getJobNumber() {
		return jobNumber;
	}

	public boolean hasRun() {
		return hasRun;
	}

	public String getDeploymentSpecName() {
		return String.format(Settings.DEPLOY_SPEC_FORMAT_STRING, jobNumber);
	}

	public String getSavedStateName() {
		return String.format(Settings.SAVED_STATE_FORMAT_STRING, jobNumber);
	}

	@Override
	public int hashCode() {
		return jobNumber;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Job)) {
			return false;
		}
		return ((Job)obj).jobNumber == jobNumber;
	}

}

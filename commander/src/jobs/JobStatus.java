package jobs;

public enum JobStatus {

	/**
	 * Indicates the job has completed.
	 */
	COMPLETED,
	/**
	 * Indicates the job is currently deployed and executing.
	 */
	EXECUTING,
	/**
	 * Indicates the job is deployed for execution but has not started
	 * executing yet.
	 */
	DEPLOYED,
	/**
	 * Indicates the job is waiting on a node and not intended to be executing.
	 */
	WAITING,
	/**
	 * Indicates a job is in the commander ready queue.
	 */
	ENQUEUED,
	/**
	 * Indicates a job has just been created and specified and is not ready to
	 * be enqueued.
	 */
	NEW;

}

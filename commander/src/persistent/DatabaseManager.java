package persistent;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

import global.Settings;
import jobs.Job;

public class DatabaseManager {
	
	private static File outsideDir;
	static {
		try {
			outsideDir = new File(
					DatabaseManager.class.getProtectionDomain().getCodeSource().getLocation().toURI()
					).getParentFile();
		} catch (Exception e) {
			outsideDir = null;
		}
	}

	public static void writeSavedState(Job job, byte[] data) {
		try {
			Files.write(new File(outsideDir, "/jobs/" + job.getSavedStateName()).toPath(), data, StandardOpenOption.CREATE);
		} catch (IOException e) {
			System.err.printf("Failed to write save state for job %d.%n", job.getJobNumber());
			if (Settings.DEBUG) {
				e.printStackTrace();
			}
		}
	}

	public static void writeDeploymentSpec(Job job, byte[] deploymentSpec) {
		try {
			Files.write(new File(outsideDir, "/jobs/" + job.getDeploymentSpecName()).toPath(), deploymentSpec, StandardOpenOption.CREATE);
		} catch (IOException e) {
			System.err.printf("Failed to write save state for job %d.%n", job.getJobNumber());
			if (Settings.DEBUG) {
				e.printStackTrace();
			}
		}
	}

	public static void writePassableMessage(Job job, String message, byte[] data) {
		try {
			Files.write(new File(outsideDir, "/job_data/" + message + ".dat").toPath(), data, StandardOpenOption.CREATE);
		} catch (IOException e) {
			System.err.printf("Failed to write save state for job %d.%n", job.getJobNumber());
			if (Settings.DEBUG) {
				e.printStackTrace();
			}
		}
	}

	public static void writeFinishedState(Job job, byte[] data) {
		try {
			Files.write(new File(outsideDir, "/job_data/finished_" + job.getJobNumber() + ".dat").toPath(), data, StandardOpenOption.CREATE);
		} catch (IOException e) {
			System.err.printf("Failed to write save state for job %d.%n", job.getJobNumber());
			if (Settings.DEBUG) {
				e.printStackTrace();
			}
		}
	}

	public static byte[] readSavedState(Job job) {
		try {
			return Files.readAllBytes(new File(outsideDir, "/jobs/" + job.getSavedStateName()).toPath());
		} catch (IOException e) {
			e.printStackTrace();
			return "NULL".getBytes();
		}
	}

	public static byte[] readDeploymentSpec(Job job) {
		try {
			return Files.readAllBytes(new File(outsideDir, "/jobs/" + job.getDeploymentSpecName()).toPath());
		} catch (IOException e) {
			e.printStackTrace();
			return "NULL".getBytes();
		}
	}

	public static byte[] readData(Job job, String message) {
		try {
			return Files.readAllBytes(new File(outsideDir, "/job_data/" + message + ".dat").toPath());
		} catch (IOException e) {
			e.printStackTrace();
			return "NULL".getBytes();
		}
	}

	public static byte[] readFinishedData(Job job) {
		try {
			return Files.readAllBytes(new File(outsideDir, "/job_data/finished_" + job.getJobNumber() + ".dat").toPath());
		} catch (IOException e) {
			e.printStackTrace();
			return "NULL".getBytes();
		}
	}

}

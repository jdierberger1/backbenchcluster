package scheduling;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import jobs.Job;
import network.JobLauncher;

public abstract class Scheduler implements Runnable {

	private static boolean running = true;

	public static void stop() {
		running = false;
	}

	private JobLauncher launcher;

	private Set<Machine> pausedMachines;

	public Scheduler(JobLauncher launcher) {
		this.launcher = launcher;
		pausedMachines = Collections.synchronizedSet(new HashSet<>());
	}

	/**
	 * Enqueue a job to be scheduled.
	 * @param job The job to enqueue.
	 */
	public abstract void enqueue(Job job);

	/**
	 * Handle a paused job on the given Machine.
	 * @param m The Machine that was paused.
	 */
	public abstract void handlePausedJob(Machine m);

	/**
	 * Dequeue a job to schedule
	 */
	public abstract Job dequeue();

	@Override
	public final void run() {
		while (running) {
			for (Machine machine : MachineRegistry.getMachines()) {
				if (machine.isPaused() && !pausedMachines.contains(machine)) {
					handlePausedJob(machine);
					pausedMachines.add(machine);
				} else if (!machine.isPaused() && pausedMachines.contains(machine)) {
					pausedMachines.remove(machine);
				}
				if (machine.isAvailable()) {
					Job j = dequeue();
					if (j != null) {
						machine.setRunningJob(j);
						j.setAddress(machine.getAddress());
						launcher.enqueueJob(j);
					}
				}
			}
		}
	}

}

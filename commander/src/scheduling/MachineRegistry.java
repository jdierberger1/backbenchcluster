package scheduling;

import java.net.InetAddress;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import global.Logging;
import jobs.Job;

public class MachineRegistry {

	private static final Map<InetAddress, Machine> machines = Collections.synchronizedMap(new HashMap<>());

	public static void helloMachine(InetAddress addr) {
		if (machines.get(addr) == null) {
			Logging.logf("Register new machine %s%n", addr.toString());
			machines.put(addr, new Machine(addr));
		} else {
			Logging.logf("HELLO old machine %s%n", addr.toString());
			machines.get(addr).setPaused(false);
			machines.get(addr).clearJobs();
		}
	}

	public static void addJob(InetAddress addr, Job job) {
		Logging.logf("Adding job %d to machine %s%n", job.getJobNumber(), addr);
		machines.get(addr).addJob(job);
	}

	public static void setRunningJob(InetAddress addr, Job job) {
		Logging.logf("Setting running on machine %s to %d%n", addr, job.getJobNumber());
		machines.get(addr).setRunningJob(job);
	}

	public static void pauseMachine(InetAddress addr) { 
		Logging.logf("Marking job %d paused on machine %s", machines.get(addr).getRunningJob().getJobNumber(), addr);
		machines.get(addr).setPaused(true);
	}

	public static void resumeMachine(InetAddress addr, Job job) { 
		Logging.logf("Resuming job %d on machine %s%n", job.getJobNumber(), addr);
		machines.get(addr).setPaused(false);
		machines.get(addr).setRunningJob(job);
	}

	public static void finishMachine(InetAddress addr, Job oldJob, Job newJob) { 
		if (oldJob == null) {
			System.err.println("Attempted to finish job number which does not exist");
			return;
		}
		Logging.logf("Finishing job %d on machine %s (replacing with %d)%n", oldJob.getJobNumber(), addr, newJob == null ? 0 : newJob.getJobNumber());
		machines.get(addr).removeJob(oldJob);
		machines.get(addr).setRunningJob(newJob);
	}

	public static int getNumMachines() {
		return machines.values().size();
	}

	public static int getNumFreeMachines() {
		int c = 0;
		for (Machine m : machines.values()) {
			if (m.isAvailable()) {
				c++;
			}
		}
		return c;
	}

	public static HashSet<Machine> getMachines() {
		return new HashSet<>(machines.values());
	}

	public static HashSet<Machine> getFreeMachines() {
		HashSet<Machine> frees = new HashSet<Machine>(machines.values());
		frees.removeIf(m -> !m.isAvailable());
		return frees;
	}

}

package scheduling;

import java.net.InetAddress;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import jobs.Job;

public class Machine {

	private boolean paused;

	private Set<Job> jobs;

	private Job runningJob;

	private InetAddress address;

	public Machine(InetAddress address) {
		paused = false;
		runningJob = null;
		jobs = Collections.synchronizedSet(new HashSet<>());
		this.address = address;
	}

	public InetAddress getAddress() {
		return address;
	}

	public void addJob(Job job) {
		jobs.add(job);
		job.setAddress(address);
	}

	public void setRunningJob(Job job) {
		if (job == null) {
			clearJobs();
			return;
		}
		addJob(job);
		runningJob = job;
	}

	public Job getRunningJob() {
		return runningJob;
	}

	public void removeJob(Job job) {
		jobs.remove(job);
		if (runningJob.equals(job)) {
			runningJob = null;
		}
		job.setAddress(null);
	}

	public void clearJobs() {
		runningJob = null;
		jobs.clear();
	}

	public void setPaused(boolean paused) {
		this.paused = paused;
	}

	public boolean isPaused() {
		return paused;
	}

	public boolean isAvailable() {
		return !paused && jobs.size() == 0;
	}

}

package scheduling.schedulers;

import java.util.concurrent.LinkedBlockingQueue;

import jobs.Job;
import network.JobLauncher;
import network.OutboundSender;
import scheduling.Machine;
import scheduling.Scheduler;

public class FIFOScheduler extends Scheduler {

	/**
	 * LinkedList of open socket connections.
	 */
	private LinkedBlockingQueue<Job> jobQueue;
	
	public FIFOScheduler(JobLauncher launcher) {
		super(launcher);
		jobQueue = new LinkedBlockingQueue<>();
	}

	@Override
	public void enqueue(Job job) {
		jobQueue.add(job);
	}

	@Override
	public void handlePausedJob(Machine m) {
		OutboundSender.send(m.getRunningJob(), String.format("(DELETE,%d)",m.getRunningJob().getJobNumber()), "".getBytes());
		m.getRunningJob().setRun(false);
		enqueue(m.getRunningJob());
		m.removeJob(m.getRunningJob());
	}

	@Override
	public Job dequeue() {
		return jobQueue.poll();
	}

}

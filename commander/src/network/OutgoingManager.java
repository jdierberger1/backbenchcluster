package network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

public class OutgoingManager {

	private static final ConcurrentHashMap<InetAddress, ConcurrentHashMap<Integer, Socket>> connections = new ConcurrentHashMap<>();

	private static final ConcurrentHashMap<Socket, Integer> uses = new ConcurrentHashMap<>();


	public static Socket open(InetAddress address, int port) throws IOException {
		ConcurrentHashMap<Integer, Socket> map = connections.get(address);
		if (map == null) {
			connections.put(address, map = new ConcurrentHashMap<>());
		}
		Socket socket = map.get(port);
		if (socket == null) {
			map.put(port, socket = new Socket(address, port));
		}
		uses.put(socket, uses.getOrDefault(socket, 0) + 1);
		return socket;
	}

	public static void close(Socket socket) throws IOException {
		if (socket == null) {
			return;
		}
		uses.put(socket, uses.getOrDefault(socket, 0) - 1);
		if (uses.get(socket) <= 0) {
			ConcurrentHashMap<Integer, Socket> map = connections.get(socket.getInetAddress());
			if (map == null) {
				connections.put(socket.getInetAddress(), map = new ConcurrentHashMap<>());
			}
			map.remove(socket.getPort()).getOutputStream().write("\n_quit".getBytes());
			if (socket != null) {
				socket.close();
			}
		}
	}

}

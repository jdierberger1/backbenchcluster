package network;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

import global.Logging;
import global.Settings;
import jobs.Job;

public class OutboundSender {

	public static boolean send(Job job, String header, byte[] data) {
		
		Socket socket;
		try {
			socket = OutgoingManager.open(job.getAddress(), Settings.NETWORK_PORT);
		} catch (IOException e) {
			System.err.printf("Failed to establish connection for job %d to address %s%n", job.getJobNumber(),
					job.getAddress().toString());
			if (Settings.DEBUG) {
				e.printStackTrace();
			}
			return false;
		}

		try {
			PrintStream printStream = new PrintStream(socket.getOutputStream());
			Logging.logf("Writing: %s%s%n", header, new String(data));
			printStream.println(header);
			printStream.write(data);
		} catch (IOException e) {
			System.err.printf("Failed to send job %d to address %s%n", job.getJobNumber(), job.getAddress().toString());
			if (Settings.DEBUG) {
				e.printStackTrace();
			}
		}

		try {
			OutgoingManager.close(socket);
		} catch (IOException e) {
			System.err.printf("Failed to close socket to to address %s%n", job.getAddress().toString());
			if (Settings.DEBUG) {
				e.printStackTrace();
			}
			return false;
		}

		return true;

	}

}

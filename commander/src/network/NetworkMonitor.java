package network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;

import global.Logging;
import global.Settings;

public class NetworkMonitor implements Runnable {

	private static boolean running = true;

	public static void stop() {
		running = false;
	}

	/**
	 * LinkedList of open socket connections.
	 */
	private LinkedBlockingQueue<Socket> connections;
	
	public NetworkMonitor() {
		connections = new LinkedBlockingQueue<>();
	}

	/**
	 * Get a list of connections.
	 * @return
	 */
	public LinkedBlockingQueue<Socket> getConnectionList() {
		return connections;
	}

	@Override
	public void run() {
		// Create the server socket
		ServerSocket server;
		try {
			server = new ServerSocket(Settings.NETWORK_PORT);
		} catch (IOException e) {
			System.err.printf("Failed to open socket on port %d%n", Settings.NETWORK_PORT);
			if (Settings.DEBUG) {
				e.printStackTrace();
			}
			return;
		}

		while (running) {
			// Open a connection from the socket.
			try {
				connections.add(server.accept());
				Logging.log("Got inbound connection");
			} catch (IOException e) {
				System.err.printf("Attempted to open socket, but failed for unknown reasons.%n");
				if (Settings.DEBUG) {
					e.printStackTrace();
				}
			}
		}

		// Close the socket
		try {
			server.close();
		} catch (IOException e) {
			System.err.printf("Failed to close socket on %d%n", Settings.NETWORK_PORT);
			if (Settings.DEBUG) {
				e.printStackTrace();
			}
		}

	}

}

package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import global.Logging;
import global.Settings;

public class SocketManager implements Runnable {

	private static boolean running = true;

	public static void stop() {
		running = false;
	}

	/**
	 * LinkedList of open socket connections.
	 */
	private LinkedBlockingQueue<Socket> connections;

	private ConcurrentHashMap<Socket, StringBuilder> connectionDatas;
	
	public SocketManager(LinkedBlockingQueue<Socket> connections) {
		this.connections = connections;
		connectionDatas = new ConcurrentHashMap<>();
	}

	@Override
	public void run() {
		while (running) {
			
			// On new connections opening, clear out disconnected sockets.
			Iterator<Socket> iterator = connections.iterator();
			while (iterator.hasNext()) {
				Socket socket = iterator.next();
				if (socket.isClosed()) {
					Logging.logf("Got: %s%n", connectionDatas.get(socket));

					DataManager.handleMessage(socket.getInetAddress(), connectionDatas.get(socket).toString());

					iterator.remove();
				} else {
					BufferedReader reader = null;
					try {
						reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
					} catch (IOException e) {
						if (Settings.DEBUG) {
							e.printStackTrace();
						}
					}
					
					StringBuilder data = connectionDatas.getOrDefault(socket, new StringBuilder());
					if (reader != null) {
						try {
							String line;
							while ((line = reader.readLine()) != null) {
								data.append(line);
								if (line.equalsIgnoreCase("_quit") || line.toLowerCase().endsWith("_quit")) {
									socket.close();
									break;
								}
							}
						} catch (IOException e) {
							if (Settings.DEBUG) {
								e.printStackTrace();
							}
						}
					}
					connectionDatas.put(socket, data);
				}
			}

		}
	}

}

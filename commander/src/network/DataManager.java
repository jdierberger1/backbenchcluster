package network;

import java.net.InetAddress;

import jobs.Job;
import persistent.DatabaseManager;
import scheduling.MachineRegistry;

public class DataManager {

	public static void handleMessage(InetAddress sender, String data) {
		
		String header = data.substring(0, data.indexOf(')')).replace("(", "");
		byte[] byteData = data.substring(data.indexOf(')') + 1).getBytes();
		String[] parts = header.split(",");
		switch (parts[1].toUpperCase()) {
			case "FINISHED":
				MachineRegistry.finishMachine(sender, Job.get(parts[0]), parts.length < 3 ? null : Job.get(parts[2]));
				DatabaseManager.writeFinishedState(Job.get(parts[0]), byteData);
				break;
			case "PAUSING":
				MachineRegistry.pauseMachine(sender);
				break;
			case "RESUMING":
				MachineRegistry.resumeMachine(sender, Job.get(parts[0]));
				break;
			case "HELLO":
				MachineRegistry.helloMachine(sender);
				break;
			case "PASS":
				DatabaseManager.writePassableMessage(Job.get(parts[0]), parts.length < 3 ? "" : parts[2], byteData);
				break;
			case "SAVEDRET":
				// unimplemented, for now
				break;
			case "QUERY":
				byte[] bytes = DatabaseManager.readData(Job.get(parts[0]), parts.length < 3 ? "" : parts[2]);
				OutboundSender.send(Job.get(parts[0]), "(QUERYRET," + parts[0] + ")", bytes);
				break;
		}
		
	}

}

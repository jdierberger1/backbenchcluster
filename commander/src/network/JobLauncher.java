package network;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;

import global.Settings;
import jobs.Job;
import persistent.DatabaseManager;

public class JobLauncher implements Runnable {

	private static boolean running = true;

	public static void stop() {
		running = false;
	}

	/**
	 * LinkedList of open socket connections.
	 */
	private LinkedBlockingQueue<Job> jobsToLaunch;
	
	public JobLauncher() {
		jobsToLaunch = new LinkedBlockingQueue<>();
	}

	public void enqueueJob(Job j) {
		jobsToLaunch.add(j);
	}

	@Override
	public void run() {
		while (running) {
			Iterator<Job> iterator = jobsToLaunch.iterator();
			while (iterator.hasNext()) {

				Job job = iterator.next();
				Socket socket;
				try {
					socket = OutgoingManager.open(job.getAddress(), Settings.NETWORK_PORT);
				} catch (IOException e) {
					System.err.printf("Failed to establish connection for job %d to address %s%n", job.getJobNumber(),
							job.getAddress().toString());
					if (Settings.DEBUG) {
						e.printStackTrace();
					}
					iterator.remove();
					continue;
				}

				try {
					PrintStream printStream = new PrintStream(socket.getOutputStream());
					if (job.hasRun()) {
						printStream.printf("(REQUEUE,%d)%n", job.getJobNumber());
						printStream.write(DatabaseManager.readSavedState(job));
					} else {
						printStream.printf("(ENQUEUE,%d)%n", job.getJobNumber());
						printStream.write(DatabaseManager.readDeploymentSpec(job));
						job.setRun(true);
					}
				} catch (IOException e) {
					System.err.printf("Failed to send job %d to address %s%n", job.getJobNumber(), job.getAddress().toString());
					if (Settings.DEBUG) {
						e.printStackTrace();
					}
					iterator.remove();
				}

				try {
					OutgoingManager.close(socket);
				} catch (IOException e) {
					System.err.printf("Failed to close socket to to address %s%n", job.getAddress().toString());
					if (Settings.DEBUG) {
						e.printStackTrace();
					}
					iterator.remove();
					continue;
				}
				
				iterator.remove();

			}
		}
	}

}

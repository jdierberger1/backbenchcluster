import java.io.File;
import java.util.Scanner;

import global.Settings;
import jobs.Job;
import network.JobLauncher;
import network.NetworkMonitor;
import network.SocketManager;
import scheduling.Scheduler;
import scheduling.schedulers.FIFOScheduler;

public class CommanderMain {

	public static void main(String[] args) throws Throwable {
		File outsideDir = new File(CommanderMain.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile();

		NetworkMonitor nm = new NetworkMonitor();
		JobLauncher jl = new JobLauncher();
		SocketManager sm = new SocketManager(nm.getConnectionList());
		Scheduler s = new FIFOScheduler(jl);
		spawnThreads(nm, jl, sm, s);

		boolean running = true;
		Scanner inScanner = new Scanner(System.in);
		while (running) {
			System.out.print("Enter a file to be used as the deployment spec for a job: ");
			String fileName = inScanner.nextLine();
			
			File f = new File(outsideDir, fileName);
			Job job = new Job();

			f.renameTo(new File(outsideDir, "/jobs/" + String.format(Settings.DEPLOY_SPEC_FORMAT_STRING, job.getJobNumber())));
			f = new File(outsideDir, "/jobs/" + String.format(Settings.DEPLOY_SPEC_FORMAT_STRING, job.getJobNumber()));
			
			s.enqueue(job);
			System.out.println("Job enqeueued.");
		}
		inScanner.close();

		NetworkMonitor.stop();
		JobLauncher.stop();
		SocketManager.stop();
		Scheduler.stop();
	}

	public static void spawnThreads(Runnable...runnables) {
		for (Runnable r : runnables) {
			Thread t = new Thread(r);
			t.setDaemon(true);
			t.start();
		}
	}

}

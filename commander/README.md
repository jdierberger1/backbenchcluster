# backbenchcluster commander

Backbench Cluster Computing Commander. This is the synchronizer and scheduler between various cluster computing nodes.

# Behaviors
- Message passing: running job sends message over network. VM forwards to vm-admin, vm-admin forwards to commander. Commander saves the message as a persistent file
- Message querying: running job sends query for a file, optionally from a job, over network. VM forwards to vm-admin, admin to commander. Commander accesses according to job number and requested file name.

# Packets the commander can send to the vm-admin
```
(type,opt2)
<data>
```
type - any of:
- "REQUEUE"     (indicates the commander wants to enqueue the job number given in `opt2`. the job was already running before; saved state is attached in `<data>`)
- "ENQUEUE"     (indicates the commander wants to enqueue the job number given in `opt2`. the job was never run before; the deployment spec is attached in `<data>`)
- "QUERYRET"    (indicates the commander is returning a query from job given in `opt2`. return attached in `<data>`, "DENIED" if the request is denied or "NULL" if no file exists)
- "SAVED"       (indicates the commander wants the saved state for the job given in `opt2`)
- "DELETE"      (indicates the commander wants to delete saved state for the job indicated in `opt2`)

# Packets the vm-admin can send to the commander
```
(job-number,type,opt3)
<data>
```
job number - the number of the job.
type - any of:
- "FINISHED"    (indicates this vm-admin's job is completed; job results are in `<data>`, `opt3` is the new job being executed [from the waiting folder])
- "PAUSING"     (indicates this vm-admin is switching to the user vm, the job given is paused. the data contains how long the job was running before the switch)
- "RESUMING"    (indicates this vm-admin is resuming the job given)
- "HELLO"       (indicates this vm-admin is establishing a connection for the first time; `job-number` can be anything non-empty)
- "PASS"        (indicates this vm-admin's job has sent a message to the server; `<data>` contains the message, `opt3` is the message name)
- "SAVEDRET"    (indicates this vm-admin is transmitting the saved state for the given job number; the state is in `<data>` or "NULL" if that state does not exist)
- "QUERY"       (indicates this vm-admin's job is querying for the file name given in `<data>` from the message name given in `opt3` [or "finished_NUM" for the finished state of job NUM])
# backbenchclustercomputing

To Run
I'll describe how to run the MVP distributed sum program  
The repository has 3 folders/branches.
- VM-ADMIN
- commander
- VM-Admin/Client

You'll need to run these on seperate machines, I used 5 EC2 t1.micro instances, 1 commander, 2 admin, 2 Clients
1.  Inside both admin machines, update settings.py to have the IP Address of the commander and client up to date
2.  Start up commander with `java -jar Commander-v.0.5.jar`  
3.  Enque jobs by doing `deployment_spec_1.dat` and `deployment_spec_2.dat`
4.  Ensure Client's environment is set up, use `mkdir Jobs && mkdir Queries && mkdir Incoming_Comms && mkdir Outgoing_Comms && mkdir Results`
5.  Start Client with `python3 Client.py`
6.  Start Admin with `python3 Manager/Manager.py`

The jobs should run now!

To run the simulator:
1. `python Lab_Simulator.py` (requires python 3)
2. Respond yes when asked if you want to generate new data
3. Choose a number of days to simulate
4. Check output graphs in the graphs folder
5. If you want to re-use the same data for another run, simply respond no when the simulator asks if you want to generate new data

If you want to run a different scheduling scheme, you must specify which one in the options at the top of the python file along with what interrupt policy and whether or not to switch between SRTF and LRTF.
The number of jobs is also specified in these options.

Backbench Cluster Computing.